# LAMP MknBox

## Features
- This shell script provisions a vagrant box by installing packages
- VM Description
	- 1GB RAM
	- Ubuntu 12.04 LTS 64-bit
	- Apache w/ mod_rewrite
	- Mariadb
	- Nodejs
	- PHP
	- Pear
	- XDebug
	- PHPunit
	- Composer
	- Behat
	- Mariadb 
	- Memcached
	- APC
	- Elastic search
	- RabbitMQ
	- Git
	- PECL


## Setup
The `Vagrantfile` shares 3 folders from your host machine into your VM: '/vagrant/data, "/vagrant/scripts".  If these folders don't exist they will be created when `vagrant up` is run. 

These folders are used as follows:
- vagrant
	- This is the root of the VM and the Project
- vagrant/data
	- This is where the data file are stored. 
	- datadump files will be created there.
- vagrant/logs
	- This is where application logs will be dumpped
- vagrant/script
	- This is where system wide script will be stored
	
When the VM is booted for the 1st time or recreated, the warning messages from apache about not being able to determine the ServerName will be displayed. The bootstrap file eventually fixes this as it goes through its process.  

Edit your local hosts file to point a domain to `192.168.56.103` then use that domain in your browser to hit the site your VM is serving.

## What is the provision.sh script doing?
- Updates software on the VM
- Installs necessary packages
- Creates the `devdb` Mariadb database and user
- Imports `database.sql` into the `devdb` database if the file exists
- Sets ServerName for Apache to keep it from complaining
- Enables mod_rewrite
- Allows use of .htaccess files
- Installs XDebug
- Sets PHP configuration values:
	- Turns on `display_errors`
	- Turns on `error_reporting` and sets to development values (display everything)
	- Turns on `html_errors`
	- Tells PHP about XDebug
- Restarts Apache

## Creating the database dump file
This  command, run from inside the VM, will dump the `devdb` database into a file called `database.sql` and place it into `/vagrant/data/sqldump/` which should be shared with host computer, thus it will continue to exist even if the VM is destroyed.

`mysqldump -uroot -proot devdb > /var/sqldump/database.sql`

