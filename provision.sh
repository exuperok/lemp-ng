#!/bin/sh
sudo apt-get -y update
sudo apt-get -y upgrade

#Install external dependencies: git, curl, python dependencies etc...
sudo apt-get -y install git imagemagick curl build-essential tcl8.5 re2c openssl memcached librabbitmq-dev
sudo apt-get -y install openjdk-7-jre python-software-properties python g++ make pkg-config

if [ ! -f /usr/java/default ];
then
    sudo mkdir /usr/java
    sudo ln -s /usr/lib/jvm/java-7-openjdk-amd64 /usr/java/default
fi


#Add php, mariadb, nodejs, solr, rabbitmq repositories
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo add-apt-repository -y ppa:ondrej/php5
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
sudo add-apt-repository -y 'deb http://ftp.igh.cnrs.fr/pub/mariadb/repo/10.0/ubuntu precise main'
if [ ! -f rabbitmq-signing-key-public.asc ];
then
    sudo wget -q http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
fi
sudo apt-key add rabbitmq-signing-key-public.asc
sudo add-apt-repository -y 'deb http://www.rabbitmq.com/debian/ testing main'
sudo apt-get update


#Install Apache, PHP & dependencies
sudo apt-get -y install apache2 php5 php5-dev php5-imagick php-apc php5-curl php-pear php5-mysql

# Set timezone
echo "Europe/Paris" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata


# Apache changes
if [ ! -f /var/log/webserversetup ];
then
    echo "ServerName localhost" | sudo tee /etc/apache2/httpd.conf > /dev/null
    sudo a2enmod rewrite
    sudo sed -i '/AllowOverride None/c AllowOverride All' /etc/apache2/sites-available/default

    sudo touch /var/log/webserversetup
fi

# Install amqp client for RabbitMQ
if [ ! -f /var/log/amqpsetup ];
then
     git clone git://github.com/alanxz/rabbitmq-c.git
     cd rabbitmq-c
     # Enable and update the codegen git submodule
     git submodule init
     git submodule update
     # Configure, compile and install
     autoreconf -i && ./configure && make && sudo make install
     sudo pecl install amqp
     AMQP_LOCATION=$(find / -name 'amqp.so' 2> /dev/null)
     cd ..
     sudo touch /var/log/amqpsetup
fi

# Install xhprof profiler
if [ ! -f /var/log/xhprofsetup ];
then
    sudo pecl install channel://pecl.php.net/xhprof-0.9.4
    XHPROF_LOCATION=$(find / -name 'xhprof.so' 2> /dev/null)

    sudo touch /var/log/xhprofsetup
fi


# Install Xdebug
if [ ! -f /var/log/xdebugsetup ];
then
    sudo pecl install xdebug
    XDEBUG_LOCATION=$(find / -name 'xdebug.so' 2> /dev/null)

    sudo touch /var/log/xdebugsetup
fi


# Configure PHP through php.ini file
if [ ! -f /var/log/phpsetup ];
then
    sudo sed -i '/display_errors = Off/c display_errors = On' /etc/php5/apache2/php.ini
    sudo sed -i '/error_reporting = E_ALL & ~E_DEPRECATED/c error_reporting = E_ALL | E_STRICT' /etc/php5/apache2/php.ini
    sudo sed -i '/error_reporting = E_ALL & ~E_DEPRECATED/c error_reporting = E_ALL | E_STRICT' /etc/php5/cli/php.ini
    sudo sed -i '/html_errors = Off/c html_errors = On' /etc/php5/apache2/php.ini
    echo "zend_extension='$XDEBUG_LOCATION'" | sudo tee -a /etc/php5/apache2/php.ini > /dev/null
    echo "zend_extension='$XDEBUG_LOCATION'" | sudo tee -a /etc/php5/cli/php.ini > /dev/null

    echo "extension='$XHPROF_LOCATION'" | sudo tee -a /etc/php5/apache2/php.ini > /dev/null
    echo "extension='$XHPROF_LOCATION'" | sudo tee -a /etc/php5/cli/php.ini > /dev/null

    echo "extension='$AMQP_LOCATION'" | sudo tee -a /etc/php5/apache2/php.ini > /dev/null
    echo "extension='$AMQP_LOCATION'" | sudo tee -a /etc/php5/cli/php.ini > /dev/null

    sudo touch /var/log/phpsetup
fi

#Install phpunit and dependencies
sudo pear config-set auto_discover 1
sudo pear install --alldeps pear.phpunit.de/PHPUnit
sudo pear install phpunit/DbUnit
sudo pear install phpunit/PHPUnit_Selenium
sudo pear install phpunit/PHPUnit_Story

#Install static analysis tools
sudo pear channel-discover pear.pdepend.org
sudo pear install pdepend/PHP_Depend-beta
sudo pear channel-discover pear.phpmd.org
sudo pear install --alldeps phpmd/PHP_PMD
sudo pear install PHP_CodeSniffer


#Installing composer for package management
if [ ! -f /usr/local/bin/composer/composer.phar ];
then
    sudo curl -sS https://getcomposer.org/installer | php
    sudo mv composer.phar /usr/local/bin/composer
fi

#Installing behat for Behavior Driven Development
if [ ! -f /usr/local/bin/behat ];
then
    sudo mkdir /opt/behat
    sudo chmod 777 /opt/behat/
    sudo cp /vagrant/composer.json /opt/behat/
    cd /opt/behat/
    sudo composer install
    sudo ln -s /opt/behat/bin/behat /usr/local/bin/behat
    cd /home/vagrant
fi

#Create Mariadb root profile
echo mariadb-server-5.5 mysql-server/root_password password root | sudo debconf-set-selections
echo mariadb-server-5.5 mysql-server/root_password_again password root | sudo debconf-set-selections


#install mariadb and php extension
sudo apt-get install -y mariadb-server

# Setup roject database
# database name, username & password need to be changed per project
if [ ! -f /var/log/databasesetup ];
then
    echo "DROP DATABASE IF EXISTS devdb" | mysql -uroot -proot
    echo "CREATE USER 'devdb'@'localhost' IDENTIFIED BY 'devdb'" | mysql -uroot -proot
    echo "CREATE DATABASE devddb" | mysql -uroot -proot
    echo "GRANT ALL ON devdb TO 'devdb'@'localhost'" | mysql -uroot -proot
    echo "flush privileges" | mysql -uroot -proot

    sudo touch /var/log/databasesetup
fi


#Install Nodejs & geric dependencies for testing, quality analysis and code coverage
sudo apt-get -y install nodejs
npm install -g --save-dev grunt-cli #tasks runner
npm install -g --save-dev bower #dependecny management
npm install -g --save-dev chai #TDD & BDD assertion library
npm install -g --save-dev qunitjs #TDD and BDD Framework
npm install -g --save-dev jasmine-node #TDD and BDD Framework
npm install -g --save-dev karma --save-dev #E2E testing in JS
npm install -g --save-dev karma-jasmine karma-chrome-launcher --save-dev #E2E testing in JS in the browser
npm install -g --save-dev nightwatch #E2E testing in JS
npm install -g --save-dev istanbul #code coverage for js framework
npm install -g --save-dev jshint # detect errors and potential problems in JavaScript code
npm install -g --save-dev jscs #detect copy paste pattern in js code
npm install -g --save-dev escomplex #detect high level of cyclomatic complexity in js code
npm install -g --save-dev yo #scafolding project generator for js
npm install -g --save-dev plato #source code visualization, static analysis, and complexity tool
#Installing selenium webtest runner
if [ ! -f /usr/local/bin/selenium-server/ ];
then
    wget http://selenium-release.storage.googleapis.com/2.42/selenium-server-standalone-2.42.0.jar
    sudo mv selenium-server-standalone-2.42.0.jar /opt/
    sudo ln -s /opt/selenium-server-standalone-2.42.0.jar /usr/local/bin/selenium-server
fi

#Installing headless browser for testing purposes
npm install -g --save-dev phantomjs
npm install -g --save-dev casperjs


#Install Solr
sudo apt-get -y install solr-tomcat

#Install RabbitMQ
if [ ! -f /var/log/rabbitmqsetup ];
then
    sudo sudo apt-get -y install rabbitmq-server
    sudo service rabbitmq-server start
    sudo rabbitmq-plugins enable rabbitmq_management
    sudo rabbitmqctl add_user admin password
    sudo rabbitmqctl set_user_tags admin administrator
    sudo rabbitmqctl set_permissions -p / admin ".*" ".*" ".*"
    sudo rabbitmqctl delete_user guest
    sudo service rabbitmq-server restart

    sudo touch /var/log/rabbitmqsetup
fi

# Make sure things are up and